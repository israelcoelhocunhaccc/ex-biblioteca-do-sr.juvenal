class Book < ApplicationRecord
  belongs_to :author
  has_one :reservation
  def self.search(search)
    if search
      where(["title LIKE?", "%#{search}%"])
    else
      all
    end
  end
end

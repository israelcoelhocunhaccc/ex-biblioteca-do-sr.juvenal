Rails.application.routes.draw do
  devise_for :admins
  devise_for :members
  get '/home' => 'home#index'

  root 'home#index'

  resources :reservations
  resources :librarians
  resources :users
  resources :books
  resources :authors
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

    print "Gerando nomes de Autores(Authors) ... "
    Author.create!([{name:"J.K Rowling"},
                {name:"Stephen King"},
                {name:"Charles Dickens"}])
    puts "[OK]"
    puts ""

    book_titles=["Harry Potter",
                "It - The Thing",
                "Little Dorrit"]

    i=1
    print "Gerando títulos de livros(Books) ... "
    3.times do 
        Book.create!(title:book_titles[i-1],author:Author.find(i))
        i+=1
    end
    puts "[OK]"
    puts ""

    print "Gerando nomes de usuários(User) ... "
    User.create!([{name: "Israel"},
                  {name: "Israel II"},
                  {name: "Israel III"}])
    puts "[OK]"
    puts ""

    print "Gerando nomes dos bibliotecários(Librarian) ... "
    Librarian.create!([{name: "alsoIsrael"}, 
                       {name: "alsoIsrael II"},
                       {name: "alsoIsrael III"}])
    puts "[OK]"
    puts ""

    i=1

    print "Gerando reservas(Reservations) ... "    
    3.times do 
    Reservation.create!(book:Book.find(i),
                        user:User.find(i),
                        librarian:Librarian.find(i))
    i+=1
    end
    puts "[OK]"
    puts ""